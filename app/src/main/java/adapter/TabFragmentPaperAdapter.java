package adapter;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

public class TabFragmentPaperAdapter extends FragmentPaperAdapter {
    String[] title = new String[]{
        "Tab 1","Tab 2"
    };

    public TabFragmentPaperAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;
        switch (position) {
            case 0:
                fragment = new Tab1Fragment();
                break;
            case 1:
                fragment = new Tab2Fragment();
                break;
            default:
                fragment = null;
                break;
        }
        return fragment;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return title[position];
    }

    @Override
    public int getCount() {
        return title.length;
    }
}
